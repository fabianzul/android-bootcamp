# Android-Bootcamp

## Getting started

Bienvenidos! En este repositorio estarán alojados todos los recursos de aprendizaje para el bootcamp de Android development.

A continuación podrá observar los temas tratados durante nuestro camino de aprendizaje!

<br>

## Contenidos
- [Android-Bootcamp](#android-bootcamp)
  - [Getting started](#getting-started)
  - [Contenidos](#contenidos)
- [Paradigma de Programación](#paradigma-de-programación)
- [Programación Orientada a Objetos (POO)](#programación-orientada-a-objetos-poo)
  - [Definición](#definición)
- [SOLID](#solid)
  - [Definición](#definición-1)
- [Programación Funcional](#programación-funcional)
- [Git](#git)

***
<br>
<br>

# Paradigma de Programación
Antes de empezar con otros temas fundamentales, creo que lo primero es recordar un poco nuestros primeros pasos...
 
Cuando empezamos en el mundo de la programación, nos enfrentamos a la manera de encontrar solución a un problema usando algún lenguaje con sus reglas (a esta construcción de instrucciones o pasos ordenados para dicha solución, le llamamos algoritmo) para que nuestro equipo de cómputo pueda entender lo que queremos hacer y así ejecutar esas instrucciones que escribimos. A medida que vamos avanzando en la escritura de código para elaborar nuestros algoritmos nos encontramos con que hay diferentes formas de abordar el diseño o estructura de lo que estamos escribiendo, por lo tanto podríamos decir que hay múltiples formas de ordenar nuestro código basándonos en principios y/o reglas que nos ayudarán a optimizar desde múltiples niveles el funcionamiento de nuestro programa (con esto me refiero al mantenimiento, desempeño, forma de estructurar nuestro código, entre otros)... A esto le podemos llamar ***Paradigmas de Programación***.

***
<br>
<br>

# Programación Orientada a Objetos (POO)

## Definición
Con la definición anterior, solo queda decir que este es un paradigma de programación. Listo, eso es todo... ¿no es suficiente definición? 😁 Pues NO! vamos paso a paso con la programación orientada a objetos (que obviamente es un paradigma, sobraba decirlo).
En primer lugar, y teniendo en cuenta la definición de ***[Paradigma de programación](#paradigma-de-programación)***, podemos decir que la programación orientada a objetos es un conjunto específico de guías que nos van a permitir escribir nuestro código modelando lo que queremos que haga nuestra aplicación pensando en cómo se representaría de una forma más cercana a la vida real... 🤷‍♂️🤦‍♂️ ¿y esto qué quiere decir? pues que de una u otra forma vamos a empezar a dividir nuestro código en unidades que vamos a llamar clases, estas clases vienen a ser una representación informática de lo que nuestro trozo de código vendría a ser si estuviéramos viendole en la vida real. Cómo esto sigue siendo complejo de entender, vamos a recurrir a los ejemplos más adelante. 🙌

Primero veamos los elementos que componen la POO:
- ***Clases:*** son algo así como moldes que nos permitiran construir objetos en nuestro programa, cómo tener un molde para hacer télefonos (los teléfonos serían nuestros objetos).
- ***Propiedades:*** Son las caracteristicas de una clase, por ejemplo podemos tener en el molde de télefonos el tamaño, la marca, el número, operador.
- ***Métodos:*** son las acciones que puede realizar una clase. Según el ejemplo anterior, la clase télefono puede llamar, colgar, encenderse, apagarse.
- ***Objetos:*** Son elementos que tienen propiedades y comportamientos (acciones que pueden realizar), A esto le podemos llamar "instancias" de una clase; son elementos construidos por nuestro molde.

También debemos entender que la POO posee unos pilares en los que se basa para cumplir su objetivo cómo paradigma:
- ***Abstracción:*** Es cuando logramos extraer conceptualmente los datos de un objeto que queremos aterrizar al código y así generar el molde para poder instanciar más objetos.
- ***Encapsulamiento:*** Es la forma en que nos aseguramos de cómo y a qué le vamos a dar acceso a las propiedades y/o métodos de una clase.<br>
Ejemplo:
    ```
    //podemos valernos de modificadores de acceso
    class Telefono(param1: String,param2: Int,param3: String){
        var marca: String = param1
        var numero: Int = param2
        private var puertoUSB: String = param3

        fun llamar(num: Int){
            println("llamando a " + num)
        }

        fun colgar(num: Int){
            println("colgando a " + num)
        }

        private fun flashear(){
            println("flasheando y borrando datos del telefono")
        }
    }
    ```
    para este caso, mediante el modificador *private* evitaremos que la función *flashear* sea usada desde una instancia y solo pueda ser visible dentro de la misma clase o accedida através de otro método público dentro de dicha clase. Si no hay modificadores, por defecto serán públicos (se puede acceder a ellos desde la instancia de clase (objeto)).

- ***Herencia:*** Es la manera en que podemos crear clases a partir de otras, por ejemplo, si tuvieramos una clase que se llame télefono y quisieramos crear tipos de télefonos, podriamos heredar de la clase telefono dichas caracteristicas en una clase hija que se llame TelefonoCelular o en otra que se llame TelefonoFijo.
Ejemplo:
    ``` 
    fun main() {
        val telefonoCelular = TelefonoCelular("Motorola",3123333434,"USB1")
        println("telefonoCelular: " + telefonoCelular.marca)
        telefonoCelular.tomarFoto()
    }

    open class Telefono(param1: String,param2: Long,param3: String){
        var marca: String = param1
        var numero: Long = param2
        private var puertoUSB: String = param3

        fun llamar(num: Long){
            println("llamando a " + num)
        }

        fun colgar(num: Long){
            println("colgando a " + num)
        }

        private fun flashear(){
            println("flasheando y borrando datos del telefono")
        }
    }

    class TelefonoCelular: Telefono{
        constructor(param1: String,param2: Long,param3: String) : super(param1, param2, param3)
        fun tomarFoto(){
            println("Foto capturada desde camara")
        }
    }
    ```
    para este caso, tenemos una clase padre para los Teléfonos en general, pero queremos crear una clase hija que tenga las caracteristicas adicionales que tiene un telefono celular, por ejemplo el de tomar fotos, así que recurrimos a la herencia.

- ***Polimorfismo:*** La palabra lo dice todo: *"muchas formas"*, y este pilar lo usamos para crear métodos con el mismo nombre que se comporten de manera diferente en una clase. <br>
Ejemplo:
    ```
    fun main() {
        val telefonoCelular = TelefonoCelular("Motorola",3123333434,"USB1")
        println("telefonoCelular: " + telefonoCelular.marca)
        telefonoCelular.tomarFoto()
        telefonoCelular.llamar(3144444444)
        val telefonoSatelital = TelefonoStelital("Motorola",3155555555,"USB2")
        println("telefonoSatelital: " + telefonoSatelital.marca)
        telefonoSatelital.llamar(3148888888)
    }

    open class Telefono(param1: String,param2: Long,param3: String){
        var marca: String = param1
        var numero: Long = param2
        private var puertoUSB: String = param3

        open fun llamar(num: Long){
            println("llamando a " + num)
        }

        fun colgar(num: Long){
            println("colgando a " + num)
        }

        private fun flashear(){
            println("flasheando y borrando datos del telefono")
        }
    }

    class TelefonoCelular: Telefono{
        constructor(param1: String,param2: Long,param3: String) : super(param1, param2, param3)
        override fun llamar(num: Long){
            println("Llamando vía red celular al número $num")
        }
        fun tomarFoto(){
            println("Foto capturada desde camara")
        }
    }
    
    class TelefonoStelital: Telefono{
        constructor(param1: String,param2: Long,param3: String) : super(param1, param2, param3)
        override fun llamar(num: Long){
            //llamar vía satelite
            println("Llamando via satélite al dispositivo $num")
        }
    }
    ```
    En el ejemplo anterior podemos usar el polimorfismo para sobreescribir métodos de una clase padre mediante el operador *override* y así poder usar una función cómun de diferentes maneras.

Ahora sí, vamos a los ejemplos:<br>
Resulta que necesitamos crear una aplicación para que nuestra computadora pueda controlar un teléfono celular vía usb, esto implica que nuestro código debe permitirnos conectarnos a un teléfono, realizar una llamada, colgarla, entre otras funciones.
Cuando nos enfrentamos a este tipo de programa, sin conocer este paradigma, recurriríamos a realizar un algoritmo de manera estructurada (la programación que aprendimos en nuestros primeros pasos, donde podemos llamar procedimientos cada vez que los necesitemos):


```
fun main() {
  fun conectarnosAlTelefonoViaUSB(puertoUSB: String){
      //Ejecuta una serie de instrucciones para que nuestro equipo pueda utilizar el hardware para hacer llamadas telefonicas.
      println("Conectado via usb " + puertoUSB)
  }

  fun llamar(num: Long){
      //funcion que recibe un numero para llamar utilizando el telefono conectado.
      println("Llamando a "+num)
  }

  fun colgar(num: Long){
      //funcion que recibe un numero para terminar la llamada utilizando el telefono conectado.
      println("Colgando a "+num)
  }

  conectarnosAlTelefonoViaUSB("USB2")
  llamar(3199999999)
  colgar(3199999999)
}
```

Viendo lo anterior, el problema está solucionado, podemos conectarnos al teléfono, llamar y colgar de manera procedimental... ¿Pero qué pasa si necesito datos adicionales para saber en especifico la marca del teléfono o si necesito conectarme a más teléfonos por usb para realizar llamadas simultáneas?... 🤔 podríamos hacerlo sin ningún problema de manera procedimental, pero también podemos facilitar las cosas usando Objetos! y ahí es donde podemos ver cómo funciona el paradigma de POO. Veamos un ejemplo:

```
class Telefono(param1: String,param2: Int,param3: String){
    var marca: String = param1
    var numero: Int = param2
    var puertoUSB: String = param3

    fun llamar(num: Int){
        //funcion que recibe un numero para llamar utilizando el telefono conectado.
        println("llamando a " + num)
    }

    fun colgar(num: Int){
        //funcion que recibe un numero para terminar la llamada utilizando el telefono conectado.
        println("colgando a " + num)
    }
}

fun main() {
    var telefonoA = Telefono("Motorola",3198888888,"USB1")
    var telefonoB = Telefono("Samsung",3197777771,"USB2")
    var telefonoC = Telefono("Sony",3199999992,"USB3")

    telefonoA.llamar(3210909090)
    telefonoB.llamar(3317777790)
    telefonoC.llamar(3110977097)

    telefonoA.colgar(3210909090)
    telefonoB.colgar(3317777790)
    telefonoC.colgar(3110977097)

    println(telefonoA.marca) //nos debe mostrar Motorola en la consola

    //tambien podemos llamar del telefonoA a el telefonoC
    telefonoA.llamar(telefonoC.numero)
}
```

En un inicio se ve extraño, pero vemos que ahora podemos usar un molde para crear nuestros teléfonos en el código. A ese molde le llamamos clase, la clase va a contener la información de los atributos de nuestro teléfono (numero,marca,puertoUSB) que las podemos reconocer por que son variables que guardan algún valor y describen una característica del teléfono, por otro lado tenemos los métodos que son funciones que puede realizar nuestro teléfono (llamar, colgar); y hay algo especial que son los parámetros de la clase, a esta le llamamos constructor (en otras lenguajes aparacerá como una función dentro de la clase, incluso en kotlin se pueden usar la plabra reservada constructor para constructores secundarios). El constructor es una función especial que nos permite crear instancias de nuestra clase y cuando nos referimos a crear instancias es como "crear copias" de ese molde con sus características únicas y a esas instancias les llamamos Objetos! la base de la POO. Ahora, cada que quisiéramos crear un teléfono simplemente recurrimos al constructor del molde para que cree nuestro objeto.
Aunque lo que vemos está escrito en kotlin, no se diferencia mucho del uso en cualquier lenguaje que nos permita trabajar en este paradigma.
 
Como podemos ver, la idea central de la POO es aterrizar o adecuar nuestro código como si fuera un objeto del mundo real con sus características y acciones (atributos y métodos). y en base a ese modelo/molde poder instanciar o crear cuantos objetos queramos.


***
<br>
<br>

# SOLID

## Definición
A medida que el desarrollo de aplicaciones ha ido avanzando, al parecer el mero hecho de usar paradigmas de programación para mejorar la organización, diseño, y/o mantenimiento de nuestro código no es garantía de que existan aplicaciones con una arquitectura correcta o fácil de mantener.
(Recordemos que la arquitectura en una aplicación es el conjunto de prácticas recomendadas, técnicas y patrones que se usan para darle estructura a nuestro código... 🙆‍♂️)
Debido a esto, Robert C. Martin propuso 5 principios que nos van a ayudar para crear aplicaciones fáciles de leer y de mantener a largo plazo. De ahí podemos obtener la definición del  acrónimo SOLID:

- ## S: Single Responsibility Principle
   Este punto es importante... si volvemos al concepto de clase que vimos anteriormente, lo que nos sugiere este principio es que una clase debe encargarse de una única cosa. Cuando las clases adquieren más responsabilidades empiezan a acoplarse en muchos niveles (esto quiere decir que se empieza a relacionar, por ejemplo, no solo con funciones que cambien atributos dentro de la misma clase, si no con otras que intervienen en bases de datos o en tratamiento de archivos para guardar información del objeto) y la idea es que ese acoplamiento sea evitado. 🙄 ¿cómo lo evitamos? veamos un ejemplo:

   ```

    class Phone(param1: String,param2: Long,param3: String){
        var brand: String = param1
        var number: Long = param2
        var usbPort: String = param3

        fun getBrand(){
            return this.brand
        }
        
        fun getNumber(){
            return this.number
        }
        
        fun getUsbPort(){
            return this.usbPort
        }
        
        fun savePhone(phone: Phone){
            //funcion que guarda en base de datos un objeto de tipo Phone.
            println("guardado en base de datos " + phone)
        }
        
        fun getPhone(number: Long){
            //funcion que trae desde base de datos el objeto Phone que coincida con el numero que pasamos por parametro.
            println("consultado en base de datos " + number)
        }
    }
    ```
    Si observamos bien el código anterior, estamos violando el principio, puesto que estamos cargando de varias responsabilidades a la clase. Una, la consulta de sus atributos y por otro lado su almacenamiento y consulta en base de datos. El principio nos dice que solo debemos tener una responsabilidad, una unica función para la clase. Si hay más responsabilidades, es problale que el código en un futuro se vuelva insostenible puesto que si hay que realizar algún cambio en el almacenamiento, es probable que debamos cambiar multiples partes en él mismo. 
    ¿y cómo lo solucionamos? pues una clase, una función. Veamos el ejemplo:

    ```
    class Phone(param1: String,param2: Long,param3: String){
        var brand: String = param1
        var number: Long = param2
        var usbPort: String = param3

        fun getBrand(){
            return this.brand
        }
        
        fun getNumber(){
            return this.number
        }
        
        fun getUsbPort(){
            return this.usbPort
        }
    }

    class PhoneDB(){
        fun savePhone(phone: Phone){
            //funcion que guarda en base de datos un objeto de tipo Phone.
            println("guardado en base de datos " + phone)
        }
        
        fun getPhone(number: Long){
            //funcion que trae desde base de datos el objeto Phone que coincida con el numero que pasamos por parametro.
            println("consultado en base de datos " + number)
        }
    }
    ```
    De esta manera podemos asegurarnos de que las clases tengan solo una responsabilidad y hacer nuestro código más fácil de leer, entender y mantener.

  
- ## O: Open-Closed Principle
  - Este principio nos habla sobre la forma en que usamos las clases, para ser más explicativos, por definición se dice lo siguiente: Las clases deberían estar abiertas a su extensión pero cerradas a su modificación.
  🤷‍♂️
  Aquí vamos a jugar un poco con la manera en como una clase debería de hacer un uso adecuado de la herencia (o eso creo 🤣); Pues cuando decimos que las clases deben estar *'abiertas a su extensión'*, es lo mismo que decir que deberiamos usar clases que extiendan de esta para poder obtener la información que necesitamos y  al decir *'cerradas a su modificación'*, pues precisamente para eso las extendemos para no tener que modificar ningún tipo de información directamente en la clase... como siempre, vienen los ejemplos al rescate:
    ```
    class PhoneDB(mySqlConection: MySQLConnection){
        fun savePhone(phone: Phone){
            println("guardado en base de datos " + phone)
        }
        
        fun getPhone(number: Long){
            println("consultado en base de datos " + number)
        }
    }

    ```
    En el ejemplo anterior vemos la clase que guarda en base de datos objetos del tipo Phone... pero hay algo mal con la clase respecto al principio ***"Open-Closed"*** y es que no se cumple 😲
    El hecho de que tengamos la clase atada a un objeto de conexión para mySQL nos pone en aprietos cuando tengamos que migrar a otra base de datos como MongoDB. En este caso, si usamos bien el principio, lo que deberiamos hacer es pasarle a nuestra clase una Interfaz que se encargue de manejar las conexiones, así no tenemos que cambiar nada en la clase PhoneDB, manteniendo su independentia, si no que toda la parte de conexión quedaría encapsulada en una interfaz que va a tener las opciones para multiples conexiones a diferentes tipos de bases de datos.
    Volvemos al ejemplo:

    ```
    class PhoneDB(storageConnection: StorageInterface){
        fun savePhone(phone: Phone){
            println("guardado en base de datos " + phone)
        }
        
        fun getPhone(number: Long){
            println("consultado en base de datos " + number)
        }
    }
    ```

- ## L: Liskov Substitution Principle
  - Por este lado que aunque suena muy extraño, lo que nos dice este principio es que una clase hija (y acá vamos a la herencia de nuevo) debería funcionar si la sustituimos por su clase padre sin errores... 😓 
  Cuando estamos trabajando con clases o interfaces, sabemos que una interfaz compone a una clase y le extiende el comportamiento, lo mismo cuando una clase extiende o hereda de otra clase también se le extiende el comportamiento... Si nosotros usamos una clase hija, al momento de instanciarla como un objeto que esté definido del tipo de la clase padre no debería de provocarnos ningún error. Sigue siendo complicado pero un ejemplo simple es la declaración de un List y construirla como un objeto del tipo ArrayList (Arraylist es una clase que implementa o extiende de List):
    ```
    fun main() {
        val arrayList: List<String> = ArrayList<String>()
    }
    ```
- ## I: Interface Segregation Principle
  - Este principio es relativamente más sencillo de entender que los demás. 🤣🤣
  Básicamente nos indica que debemos estar atentos de no añadir funcionalidades adicionales a una interfaz provocando que estas tengan un gran tamaño complicando su lectura y mantenimiento.

    Por ejemplo supongamos que necesitamos probar nuestros telefonos que tenemos conectados vía USB, y declaramos una interfaz que se puede implementar para poder realizar esas pruebas:

    ```
    interface PhoneService {
        fun testLightSensor() {
            println("probando el sensor de luz del telefono")
        }
        fun testProximitySensor() {
            println("probando el sensor de proximidad del telefono")
        }
    }
    ```
    Aquí le estamos fallando a éste principio, por que es posible que en algunos telefonos no necesitemos probar uno u otro sensor, por tanto si queremos aplicarlo bien, debemos separar estas funcionalidades en interfaces adicionales:

    ```
    interface testLightSensorService {
        fun testLightSensor() {
            println("probando el sensor de luz del telefono")
        }
    }

    interface testProximitySensorService {
        fun testProximitySensor() {
            println("probando el sensor de proximidad del telefono")
        }
    }
    ```

- ## D: Dependency Inversion Principle
  - La última! casi que no! 🤯
  Salgamos rápido de esto: No debemos depender de implementaciones de clases concretas, si no de sus abstracciones (si, otra vez a usar interfaces).
  <br>
    Supongamos que vamos a obtener objetos de tipo Phone de algún servicio REST:

    ```
    class PhoneREST(httpService: HTTPService){
        this httpService = httpService

        fun getPhoneREST(){
            println("Usar httpService para obtener un objeto del tipo Phone")
        }
    }
    ```
    Nuevamente estamos incumpliendo este principio... Ya qué nuestra clase está dependiendo de otra clase de bajo nivel como la que se encarga de gestionar las peticiones Http (HTTPService).<br>
    ¿Y cómo nos adaptamos a este principio? Pues lo primero es pasar a depender de abstracciones que nos van a ayudar en caso de que nuestra librería para el HTTPService cambie en algún momento. Esto debido a que vamos a estar en una especie de contrato con una interfaz y no con una clase concreta, y esa interfaz podrá usar otras librerías sin afectar la implementación que tengamos con nuestra clase PhoneREST:

    ```
    interface HttpServiceInterface {
        func get(){}; 
        func post(){}; 
    }

    class HttpService implements HttpServiceInterface { 
        // implementaciones
    }

    class PhoneREST(httpService: HTTPServiceInterface){
        this httpService = httpService

        fun getPhoneREST(){
            println("Usar httpService para obtener un objeto del tipo Phone")
        }
    }
    ```
    De esta manera podemos pasar al constructor la interfaz que tendrá la abstracción de lo que debe hacer el servicio HttpService.<br>
    A esto también se le suel decir ***"Inyección de dependencias"***.

***
<br>
<br>

# Programación Funcional

Bueno, lo de siempre, es otro paradigma de programación 🤣.
Pero vale, ésta vez podemos pensar en que es una forma que nos va a ayudar a escribir código usando principalmente funciones, de esta manera permitirnos reusar código de manera óptima y así escribir menos...
<br>
Hay algunos conceptos interesantes a tratar.
- **Funciones Puras:** es un tipo de función que no puede modificar valores externos y además los mismos argumentos que recibe son los mismos que devuelve.<br>
Ejemplo:
    ```
    fun add(a: Int, b: Int): Int {
        return a + b
    }
    ```
    Podemos observar que el retorno de la función depende unicamente de los mismos parametros de entrada sin ser módificados.

- **Funciones de orden superior:** Son funciones que toman como parámetros funciones y devuelven otras funciones, de esta manera podemos tratarlas como a cualquier variable.
Ejemplo:
    ```
    fun calculate(numA: Int, numB: Int, f: (Int, Int) -> Int) : Int {
        return f(a, b)
    }
    ```
    En las funciones de orden superior enviamos funciones como párametros y la funcion de orden superior se encarga de retornar el resultado de esa funcion.
- **Inmutabilidad:** Quiere decir que un valor no puede ser cambiado asegurandonos que el sistema mantenga su estado original y un mayor control sobre las variables.<br>
Ejemplo:
    ```
    class Person {
        private final name: String
        private final age: Int

        // ...
    }
    ```
    Nos centramos en definir párametros como *final* para que no cambien en el ciclo de vida de nuestra aplicación, evitando modificaciones externas y consevando su estado.


En resumen, el objetivo de usar este paradigma es evitar cambios de estado en nuestras aplicaciones y hacer explicitas las transformaciones de datos.

Veamos un ejemplo:

```
fun operar(v1: Int, v2: Int, fn: (Int, Int) -> Int) : Int {
    return fn(v1, v2)
}

fun sumar(x1: Int, x2: Int) = x1 + x2

fun restar(x1: Int, x2: Int) = x1 - x2

fun multiplicar(x1: Int, x2: Int) = x1 * x2

fun dividir(x1: Int, x2: Int) = x1 / x2

fun main(parametro: Array<String>) {
    val resu1 = operar(2, 6, ::sumar)
    println("La suma de 2 y 2 es $resu1")
    val resu2 = operar(5, 8, ::multiplicar)
    println("La multiplicación entre 5 y 8 es $resu2")
}

```


***
<br>
<br>


# GIT
Resulta que en nuestra vida de programadores nuestros ficheros permanecen variando constantemente, estamos propensos a equivocaciones y quisieramos tener el poder de poder recordar cómo estaba nuestro código antes de estropearlo... pues de la mano del creador de linux, llegó a nuestras vidas un software que nos da la magía para ello: ***Git*** es un software para controlar las versiones de nuestros ficheros y que de forma local guardará uno a uno los cambios que realicemos sobre ellos. Si cometemos un error podremos regresar a una versión anterior, podremos hacer un "fix" cuando encontermos un bug en nuestro código, podremos crear "bifurcaciones" o copias iguales de nuestro código para hacer modificaciones sin afectar nuestra "rama" principal y luego fusionarlas... Git es un software que parece simple, pero que es realmente mágico.
<br>
Y realmente es sencillo empezar a usarlo. Git tiene un interpréte de comandos que nos facilitará la vida al momento de interactuar con sus funcionalidades, por tanto lo ideal es aprender los comando más comunes para nuestro día a día:
- **git init:** *Inicializar un repositorio*
- **git status:** *Estar atentos a cambios en nuestro repositorio*
- **git add:** *Añadir archivos al staged*
- **git commit:** *Confirmar los cambios en nuestros archivos*
- **git push:** *Subir nuestros cambios a un origen remoto*
- **git checkout -b nombreRama:** *Crear una nueva rama*
- **git diff:** *Muestra las modificaciones en nuestro repositorio*
- **git show:** *Nos muestra el último commit*
- **git rm:** *Elimina un archivo sin borrar su historial del control de versiones*
- **git reset head:** *Quita los archivos del staging o los devuelve a su estado anterior para no incluirlos en el commit; luego del commit podemos volver a añadirlos con git add*
- **git fetch:** *Traemos actualizaciones del repositorio remoto para tenerlas localmente*
- **git merge:** *Nos sirve para mezclar los cambios que traemos del repositorio remoto en nuestra directorio de trabajo*
- **git pull:** *Es como hacer git fetch y git merge al mismo tiempo*
- **git stash:** *Guardamos temporalmente nuestro trabajo para poder cambiar de rama o relizar alguna confirmación y luego los podemos volver a recuperar con **git stash pop**.*
- **git cherry-pick:** *Nos permite mandar commits especificos a una rama sin necesidad de pasar por una mezcla de ramas. Se suele hacer cuando hay bugs en producción que necesitan ser corregidos rápidamente*
- **git clone:** *Nos permite clonar un repositorio remoto en nuestro entorno local*
- **git add remote URL:** *Agrega un repositorio remoto a nuestro repo local*


***
<br>
<br>